# JMustache

## Introduction

JMustache is a zero-dependency implementation of the Mustache template system. It extends the tags in a template by using values provided in a hash or object.

## Demo

<img src="./gif/video_EN.gif"/>

## How to Install

```
ohpm install mustache 
ohpm install @types/mustache --save-dev // Install @types/mustache to prevent import syntax errors due to missing type declarations in the mustache package.

```
For details about the OpenHarmony ohpm environment configuration, see [OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

## How to Use

**Mustache.render** is the main API used for parsing.

Data

Most of the following examples use the code block data listed below. Additional data will be provided separately if any.

```
    name: "cai",
    msg: {
      sex: "male",
      age: "66",
      hobby: "reading"
    },
    focus: "<span>sleep<span>",
    subject: [" Ch ", " En ", " Math ", " physics "],
    moreInfo: []
```

### Variables

The most basic tag type is a simple variable. A `{{name}}` tag renders the value of the `name` key in the current context. If there is no such key, nothing is rendered.

By default, all variables are HTML escaped. To render unescaped HTML, use the triple mustache: `{{{name}}}`. You can also use `&` to unescape a variable.

To change HTML escaping behavior globally (for example, to template non-HTML formats), you can override Mustache's escape function. For example, to disable all escaping, use `Mustache.escape = function(text) {return text;};`.

If you want `{{name}}` not to be interpreted as a mustache tag, but rather to appear exactly as `{{name}}` in the output, you must change and restore the default delimiter. For more information, see **Custom Delimiters** below.

Input:

```
NAME:{{name}}
EMPTY:{{nothing}}
{{focus}}
{{{focus}}}
{{&focus}}
```

Output:

```
NAME:cai
EMPTY:
&lt;span&gt;sleep&lt;span&gt;
<span>sleep<span>
<span>sleep<span>
```

### Object Attributes

`.` is used to access object attributes.

Input:

```
SEX:{{msg.sex}};AGE:{{msg.age}}
```

Output:

```
SEX:male;AGE:66
```

### Sections

Sections render blocks of text zero or more times, depending on the value of the key in the current context.

A section begins with a pound key (`#`) and ends with a backslash (`\`). That is, `{{#subject}}` begins a `subject` section, whereas `{{/subject}}` ends it. The text between the two tags is called referred to as that section's block.

The behavior of the section is determined by the value of the key.

If the `subject` key does not exist, or exists and its value is `null`, `undefined`, `false`, `0`, `NaN`, or an empty string or empty list, the block is not rendered.

Input:

```
{{#msg}}SEX:{{sex}};AGE:{{age}};HOBBY:{{hobby}}{{/msg}}
MOREINFO:{{#moreInfo}}cannot display{{/moreInfo}}
```

Output:

```
SEX:male;AGE:66;HOBBY:reading
MOREINFO:
```

### Non-Empty lists

If the `persons` key exists and its value is not `null`, `undefined`, or `false`, or an empty list, the block is rendered one or more times.

When the value is a list, the block is rendered once for each item in the list. The context of the block is set to the current item in the list for each iteration. In this way, the collections can be traversed.

Data:

```
"persons": [
    { "name": "CAI" },
    { "name": "LIU" },
    { "name": "ZHOU" }
  ]
```

Input:

```
{{#persons}}
{{name}}
{{/persons}}
```

Output:

```
CAI
LIU
ZHOU
```

### Enums

`.` can be used to traverse a string array.

Input:

```
{{#subject}}{{.}}{{/subject}}
```

Output:

```
 Ch  En  Math  physics 
```

### Functions

The parsed data can be a value or a function.

Data:

```
fun: function () {
    return 2 + 4;
 }
```

Input:

```
FUN:{{fun}}
```

Output:

```
FUN:6
```

### if else

`{{^section}}` is opposite to `{{#section}}`. The block of an inverted section is rendered only when the value of the section's tag is `null`, `undefined`, `false`, *falsy*, or an empty list.

if

Input:

```
EMPTY-ARR:{{#moreInfo}}empty-arr{{/moreInfo}}\nARRAY: {{#subject}}arr {{/subject}}\n
```

Output:

```
EMPTY-ARR:
ARRAY:arr arr arr arr
```

else

Input:

```
EMPTY-ARR:{{^moreInfo}}empty-arr{{/moreInfo}}\nARRAY: {{^subject}}arr {{/subject}}\n
```

Output:

```
EMPTY-ARR:empty-arr
ARRAY:
```

### Comments

Comments begin with the exclamation mark `!` and are ignored.

Input:

```
{{!name}}.
```

Output:

```
 .
```

### Partials

Partials begin with the greater than sign `>`. A complex template can be split into multiple partials for easy use.

Extra data:

```
template: "{{#msg}}SEX:{{sex}};AGE:{{age}};HOBBY:{{hobby}}{{/msg}}"
```

Input:

```
NAME:{{name}}\nINFO:\n{{>template}}
```

Output:

```
NAME:cai
INFO:
SEX:male;AGE:66;HOBBY:reading
```

### Custom Delimiters

You can define delimiters by modifying Mustache tags or directly use the delimiters during parsing.

Input:

```
Mustache.render(NAME:{{name}}\nNAME:<%name%>\n, this.data, {}, ['<%', '%>'])
```

Output:

```
NAME:{{name}}
NAME:cai
```

### Pre-parsing and Caching Templates

By default, when Mustache parses a template for the first time, it saves the parsed result in the cache. The next time it sees the same template, it skips the parsing step and renders the template faster. If you'd like, you can do this ahead of time using `Mustache.parse`.

## Available APIs

- `Mustache.render()`: parses a template.
- `Mustache.parse`: pre-parses a template.
- `mustache.clearCache()`: clears all cache templates in the default writer.

## Constraints
This project has been verified in the following versions:
- DevEco Studio: NEXT Beta1-5.0.3.806, SDK:API12 Release(5.0.0.66)

- DevEco Studio: 4.1 Canary (4.1.3.317), OpenHarmony SDK: API 11 (4.1.0.36)

- DevEco Studio: 4.0 Beta2 (4.0.3.512), SDK: API 10 (4.0.10.9)

- DevEco Studio: 3.1 Beta2 (3.1.0.400), SDK: API 9 Release (3.2.11.9)
## Directory Structure

```
|---- Jmustache  
|     |---- entry  # Sample code
|     |---- Jmustache  # JMustache library
|           |---- index.ets  # External APIs
|           |---- src
|                 |---- ets
|                       │---- components
|                             |---- mustache.ets # Used to parse templates
|     |---- README.md  # Readme                   
|     |---- README_zh.md  # Readme                   
```

## How to Contribute

If you find any problem when using JMustache, submit an [issue](https://gitee.com/openharmony-sig/jmustache/issues) or a [PR](https://gitee.com/openharmony-sig/jmustache/pulls).

## License

This project is licensed under [Apache LICENSE](https://gitee.com/openharmony-sig/jmustache/blob/master/LICENSE).
