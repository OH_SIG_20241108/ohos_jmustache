## 2.0.0

1.适配DevEco Studio:3.1 Beta2(3.1.0.400), SDK:API9 Release(3.2.11.9)


## 1.0.2

- 适配DevEco Studio 3.1Beta1版本。
- 适配OpenHarmony SDK API version 9版本。

## 1.0.1

- api8升级到api9 stage模型

## 1.0.0

已实现功能：

1.对指定模板进行解析，扩展模板中的标签

2.预解析将解析缓存，也可以清除缓存

3.修改自定义分隔符